const mongoose = require("mongoose");
const logger = require("./logger");
mongoose.connection.on("error", (err) => {
  logger.error(`MongoDB connection error: ${err}`);
});

exports.connect = () => {
  mongoose
    .connect(process.env.URI_CONNECT_MONGO, {
      useCreateIndex: true,
      keepAlive: 1,
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    })
    .then(() => console.log("mongoDB connected..."));
  return mongoose.connection;
};
