const express = require('express');
const userRoutes = require('./user.route');
const router = express.Router();
/*  TEST API */
router.get('/status', (req, res) => res.send('OK'));

/* ROUTE USER */
router.use('/user', userRoutes);

module.exports = router;
